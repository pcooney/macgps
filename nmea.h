/*
 *  Created by Patrick Cooney patrick@patrickcooney.com.
 */
typedef struct DegreesMinutesT {
	int Degrees;
	float Minutes;
	//Decimal representation of degrees and minutes
	//e.g. 10 degrees and 10 minutes = 10 + 10/60 degrees = 10.17 degrees
	float DecimalDegrees;
} Degrees;

typedef struct GPGGAT {
	Degrees *Lat;
	char LatD;
	Degrees *Long;
	char LongD;
	float Alt;
} Gpgga;

typedef struct NMEAT {
	Gpgga *gpgga;
} Nmea;
