/*
 *  Created by Patrick Cooney patrick@patrickcooney.com.
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <CoreFoundation/CoreFoundation.h>
#include "nmea.h"

#define ERR_RETURN -1
#define LAT_FIELD_NUMBER 2
#define LAT_D_FIELD_NUMBER LAT_FIELD_NUMBER+1
#define LNG_FIELD_NUMBER LAT_D_FIELD_NUMBER+1
#define LNG_D_FIELD_NUMBER LNG_FIELD_NUMBER+1
#define ALT_FIELD_NUMBER LNG_D_FIELD_NUMBER+4
#define MINUTES_PER_DEGREE 60.0F

void printnmea(Nmea *nmea) {
	if (NULL != nmea  && 
		NULL != nmea->gpgga && 
		NULL != nmea->gpgga->Lat && 
		NULL != nmea->gpgga->Long
		) {
		printf("$GPS,%f%c,%f%c,%f\n", 
			nmea->gpgga->Lat->DecimalDegrees,
			nmea->gpgga->LatD,
			nmea->gpgga->Long->DecimalDegrees,
			nmea->gpgga->LongD,
			nmea->gpgga->Alt);
	}
}

Nmea* newnmea() {
	Nmea *nmeaData = (Nmea*)malloc(sizeof(Nmea));
	if (NULL != nmeaData) {
		Gpgga* gpggaData = (Gpgga*)malloc(sizeof(Gpgga));
		if (NULL != gpggaData) {
			nmeaData->gpgga = gpggaData;
		}
		else {
			//allocation of gpgga failed, clean up nmea
			free(nmeaData);
			nmeaData = NULL;
		}
	}	
	return nmeaData;
}

//Parses a float from chars
//where chars starts with the pattern: [0-9]+\.[0-9]+
//and may contain further non-numeric characters which are ignored
static float parsefloat(char* chars) {
	float result;
	return (NULL != chars &&  sscanf(chars, "%f", &result)) ? result : 0.0F;
}

//Parses an int from the first numberOfDigits digits in raw
static int parseint(char *raw, int numberOfDigits) {
	int result = 0;
	if (NULL != raw && numberOfDigits > 0) {
		int tempResult = 0;
		while (numberOfDigits > 0 && 
			*raw >= '0' && *raw <= '9') {
			tempResult = tempResult * 10 + *raw - '0';
			numberOfDigits--;
			raw++;
		}
		if (numberOfDigits == 0) result = tempResult;
	}
	return result;
}

//Parses Degrees from the string raw
//Where raw is in the format [0-9+][0-9][0-9].[0-9+]
//e.g. 1234.567, which corresponds to 12 degrees and 34.567 minutes
static Degrees *parsedegrees(char *raw) {
	Degrees *d;
	const int wholeDegreeDigits = 2;
	if (NULL != raw) {
		char *parse;
		parse = raw;
		int minutesIndex = 0;
		int pointIndex = 0;
		int degrees;
		float minutes;
		
		//find the decimal point, keeping track of number of chars skipped
		while ('.' != *parse  && '\0' != *parse) {
			parse++;
			pointIndex++;
		}
		//if found '.' and we have at least three preceding chars
		if (*parse == '.' && pointIndex - wholeDegreeDigits > 0) {
			//the minutes start two chars before the point
			minutesIndex = pointIndex - wholeDegreeDigits;
			//parse the degrees, which start at 0 and take up minutesIndex chars
			degrees = parseint(raw, minutesIndex);
			
			//set parse pointer to start of minutes
			parse = raw;
			parse += minutesIndex;
			minutes = parsefloat(parse);
			
			d = (Degrees*)malloc(sizeof(Degrees));
			if (NULL != d) {
				d->Degrees = degrees;
				d->Minutes = minutes;
				d->DecimalDegrees = degrees + (minutes / MINUTES_PER_DEGREE);
			}
		}
	}
	return d;
}

//Parses particular fields from the gga sentence and puts values into the Nmea structure
static void parsegpggafield(Nmea *data, char *raw, int fieldNumber) {
	if (NULL != data && NULL != raw && fieldNumber >= 0) {
		switch (fieldNumber) {
			case LAT_FIELD_NUMBER :
				data->gpgga->Lat = (Degrees*)parsedegrees(raw+1);
				break;
			case LAT_D_FIELD_NUMBER :
				data->gpgga->LatD = *(raw+1);
				break;
			case LNG_FIELD_NUMBER :
				data->gpgga->Long = (Degrees*)parsedegrees(raw+1);
				break;
			case LNG_D_FIELD_NUMBER :
				data->gpgga->LongD = *(raw+1);
				break;
			case ALT_FIELD_NUMBER :
				data->gpgga->Alt = parsefloat(raw+1);
				break;
		}
	}
}

//Parses comma separated gpgga data, see http://www.gpsinformation.org/dale/nmea.htm
static void parsegpgga(char *rawData, Nmea *nmeaData) {
	int fieldNumber = 0;
	if (NULL != rawData && NULL != nmeaData) {
		for (; *rawData != '\0' && *rawData != '\r'; rawData++) {
			if (*rawData == ',') {
				//comma indicates the start of a field, so inc field number
				//and call parsegpggafield to put the field into the right place
				//in the nmea struct
				fieldNumber++;
				parsegpggafield(nmeaData, rawData, fieldNumber);
			}
		}
	}
}

//Parses nmea data from rawData and puts the values into nmeaData
static void parsenmea(char *rawData, Nmea *nmeaData) {
	return parsegpgga(rawData, nmeaData);
}

static 	char *firstWord = "$GPGGA"; //first word of the gga sentence
//matches first chars in buf with firstWord
//buf isn't \0 terminated, so caller checks there are
//at least firstWordLen chars in buf
static bool matchfirstword(char* buf) {
	int i;
	int firstWordLen = strlen(firstWord);
	//compare current buffer to the start of sentence
	for (i = 0; i < firstWordLen; i++) {
		if (*(firstWord + i) != *(buf)) return false;
		buf++;
	}
	return true;
}

static char* readrawnmea(int fileDescriptor)
{
    char    buffer[512];// Input buffer
    char    *bufPtr;// Current char in buffer
	char	*nmeaStartPtr;//start of nmea sentences
    ssize_t lastBytesRead;// Number of bytes read or written
	int totalBytesRead = 0;
	int totalLinesRead = 0;
	int linesToRead = 1; //Just read the gga sentence
	bool nmeaStarted = false;
    bufPtr = buffer;
	int firstWordLen = strlen(firstWord);
	do
	{
		//read up to one less byte than the remaining room in the buffer
		lastBytesRead = read(fileDescriptor, bufPtr, &buffer[sizeof(buffer)] - bufPtr - 1);
		if (ERR_RETURN == lastBytesRead)
		{
			printf("Error reading - %s(%d).\n", strerror(errno),errno);
		}
		else if (lastBytesRead > 0)
		{
			//move temp pointer to the start of the new chars
			char *tmpBufPtr = bufPtr;
			//move buffer pointer to the end of the new chars
			bufPtr += lastBytesRead;
			totalBytesRead += lastBytesRead;
			
			//scan through the buffer and look for new lines or the first word of the nmea sentences
			while (tmpBufPtr <= bufPtr) { 
				//look for new lines
				if (nmeaStarted && *tmpBufPtr == '\n')  {
					totalLinesRead++;
					if (totalLinesRead >= linesToRead) break;
				}
				//look for the first word
				else if (!nmeaStarted &&
					totalBytesRead >= firstWordLen &&
					matchfirstword(tmpBufPtr - firstWordLen + 1))  
				{
					nmeaStarted = true;
					nmeaStartPtr = tmpBufPtr - firstWordLen + 1;
				 } 
				tmpBufPtr++;
			}
		}
	} while (totalLinesRead < linesToRead);

	*bufPtr = '\0'; //add termination
	
	//copy buffer to output string
	char *res = malloc(sizeof(buffer));
	if (nmeaStarted && NULL != res) {
		char *resCp = res;
		bufPtr = nmeaStartPtr;
		while (*bufPtr != '\0') *resCp++ = *bufPtr++;
		*resCp = '\0';
	}	
    return res;
}

//Constructs nmea struct by reading data from fileDescriptor
Nmea *getnmea(int fileDescriptor) {	
	char *raw = readrawnmea(fileDescriptor);
	Nmea *nmeaData;
	if (NULL != raw) {
		nmeaData = (Nmea*)newnmea();
		if (NULL != nmeaData) parsenmea(raw, nmeaData);
		free(raw);
	}
	return nmeaData;
}

