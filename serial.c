/*
 *  Created by Patrick Cooney patrick@patrickcooney.com.
 */
#include <fcntl.h>
#include <termios.h>
#include <CoreFoundation/CoreFoundation.h>
#include "serial.h"
#define ERR_RETURN -1

static struct termios gOriginalTTYAttrs; 

int openserialport(const char *deviceFilePath)
{
    int fileDescriptor = -1;
    struct termios  options;
    fileDescriptor = open(deviceFilePath, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fileDescriptor == -1)
    {
        printf("Error opening serial port %s - %s(%d).\n",
               deviceFilePath, strerror(errno), errno);
        goto error;
    }
    if (ioctl(fileDescriptor, TIOCEXCL) == ERR_RETURN)
    {
        printf("Error setting TIOCEXCL on %s - %s(%d).\n",
            deviceFilePath, strerror(errno), errno);
        goto error;
    }
    if (fcntl(fileDescriptor, F_SETFL, 0) == ERR_RETURN)
    {
        printf("Error clearing O_NONBLOCK %s - %s(%d).\n",
            deviceFilePath, strerror(errno), errno);
        goto error;
    } 
    // Get the current options and save them so we can restore the
    // default settings later.
    if (tcgetattr(fileDescriptor, &gOriginalTTYAttrs) == ERR_RETURN)
    {
        printf("Error getting tty attributes %s - %s(%d).\n",
            deviceFilePath, strerror(errno), errno);
        goto error;
    }

    options = gOriginalTTYAttrs;
	//set termios opttions
    cfmakeraw(&options);
    options.c_cc[VMIN] = 1;
    options.c_cc[VTIME] = 10;
    cfsetspeed(&options, B4800);   // 4800 baud rate
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
  
    // Cause the new options to take effect immediately.
    if (tcsetattr(fileDescriptor, TCSANOW, &options) == ERR_RETURN)
    {
        printf("Error setting tty attributes %s - %s(%d).\n",
            deviceFilePath, strerror(errno), errno);
        goto error;
    }
	// Success:
    return fileDescriptor;
 
    // Failure:
error:
    if (fileDescriptor != ERR_RETURN)
    {
        close(fileDescriptor);
    }
 
    return -1;
}

void closeserialport(int fileDescriptor) 
{ 
	if (tcdrain(fileDescriptor) == ERR_RETURN) 
	{ 
		printf("Error waiting for drain - %s(%d).\n", 
		strerror(errno), errno); 
	} 
	//reset termios
	if (tcsetattr(fileDescriptor, TCSANOW, &gOriginalTTYAttrs) == ERR_RETURN) 
	{ 
		printf("Error resetting tty attributes - %s(%d).\n", 
		strerror(errno), errno); 
	}	
	close(fileDescriptor); 
} 
