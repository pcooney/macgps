/*
 *  Created by Patrick Cooney patrick@patrickcooney.com.
 */
#include <sysexits.h>
#include "serial.h"
#include "nmea.h"

int main() {
	char *device = "/dev/cu.usbserial";
	int fileDescriptor;
	fileDescriptor = openserialport(device);
	Nmea *nmea = (Nmea*)getnmea(fileDescriptor);
	closeserialport(fileDescriptor);
	printnmea(nmea);
	return EX_OK;
}